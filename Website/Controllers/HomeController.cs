﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Website.Controllers
{
  public partial class HomeController : Controller
  {
    public virtual ActionResult Index()
    {
      return View();
    }

    public virtual ActionResult Test()
    {
      return View();
    }

    public virtual ActionResult Membership()
    {
      return View();
    }

    // GET: Hinduism
    public virtual ActionResult Education()
    {
      return View();
    }
  }
}