﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Website.Controllers
{
  public partial class HinduismController : Controller
  {
    // GET: Hinduism
    public virtual ActionResult Religion()
    {
      return View();
    }
    
    // GET: Hinduism
    public virtual ActionResult Festivals()
    {
      return View();
    }

    // GET: Hinduism
    public virtual ActionResult MoreInformation()
    {
      return View();
    }

    // GET: Hinduism
    public virtual ActionResult Faq()
    {
      return View();
    }

    public virtual ActionResult Education()
    {
      return View(MVC.Home.Views.Education);
    }
  }
}