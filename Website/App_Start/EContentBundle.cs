﻿using WebExtras.Core;

namespace Website
{
  public enum EContentBundle
  {
    [StringValue("css-main")]
    CssMain,

    [StringValue("js-main")]
    JsMain,

    [StringValue("modernizer")]
    Modernizer,

    [StringValue("below-ie9")]
    BelowIE9
  }
}