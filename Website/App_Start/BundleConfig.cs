﻿using SquishIt.Framework;
using SquishIt.Less;
using WebExtras.Core;

namespace Website
{
  public static class BundleConfig
  {
    /// <summary>
    ///   Returns the CSS cache Url based on the given content bundle
    /// </summary>
    /// <param name="bundle">Content bundle</param>
    /// <returns>CSS cache Url</returns>
    public static string ResolveCssPath(EContentBundle bundle)
    {
      return string.Format("~/Assets/Css/{0}", bundle);
    }

    /// <summary>
    ///   Returns the JS cache Url based on the given content bundle
    /// </summary>
    /// <param name="bundle">Content bundle</param>
    /// <returns>JS cache Url</returns>
    public static string ResolveJsPath(EContentBundle bundle)
    {
      return string.Format("~/Assets/Js/{0}", bundle);
    }

    // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
    public static void RegisterBundles()
    {
      Bundle.RegisterStylePreprocessor(new LessPreprocessor());

      // main css bundle
      Bundle.Css()
        .Add(Links.Content.bootstrap_min_css)
        .Add(Links.Content.font_awesome_less)
        .Add(Links.Content.animate_css)
        .Add(Links.Content.prettyPhoto_css)
        .Add(Links.Content.main_css)
        .Add(Links.Content.style_less)
        .AsCached(EContentBundle.CssMain.ToString(), ResolveCssPath(EContentBundle.CssMain));

      // modernizer bundle
      Bundle.JavaScript()
        .Add(Links.Scripts.modernizr_2_6_2_js)
        .AsCached(EContentBundle.Modernizer.ToString(), ResolveJsPath(EContentBundle.Modernizer));

      // below IE 9 compatibility
      Bundle.JavaScript()
        .Add(Links.Scripts.html5shiv_js)
        .Add(Links.Scripts.respond_min_js)
        .AsCached(EContentBundle.BelowIE9.ToString(), ResolveJsPath(EContentBundle.BelowIE9));

      // main js bundle
      Bundle.JavaScript()
        .Add(Links.Scripts.jquery_1_10_2_min_js)
        .Add(Links.Scripts.bootstrap_min_js)
        .Add(Links.Scripts.jquery_isotope_min_js)
        .Add(Links.Scripts.jquery_prettyPhoto_js)
        .Add(Links.Scripts.main_js)
        .AsCached(EContentBundle.JsMain.ToString(), ResolveJsPath(EContentBundle.JsMain));
    }
  }
}